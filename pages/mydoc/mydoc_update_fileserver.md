---
title: Update Psono Fileserver
summary: "Update instruction for the Psono fileserver"
tags: [update, server]
sidebar: mydoc_sidebar
permalink: mydoc_update_fileserver.html
folder: mydoc
---

## Preamble

From time to time you want to update to the latest version. If you have a possibility to snapshot your machine, please do so.

## Update with Docker

1.  Update the docker image

	    docker pull psono/psono-fileserver:latest
    
2.  Stop old psono-fileserver

        docker stop psono-fileserver
    
3.  Start new psono-fileserver

        docker run --name psono-fileserver-new \
            --sysctl net.core.somaxconn=65535 \
            -v /path/to/modified/settings.yaml:/root/.psono_fileserver/settings.yaml \
            -v /opt/psono-shard:/opt/psono-shard \
            -d --restart=unless-stopped -p 10200:80 psono/psono-fileserver:latest
            
4.  Cleanup

	If everything works you can cleanup your containers with the following commands:
	
		docker rm psono-fileserver
		docker rename psono-fileserver-new psono-fileserver

If anything fails you should be able to start the old docker container again.


{% include links.html %}
