---
title: FAQs
tags: []
summary: "FAQs concerning the installation and configuration"
sidebar: mydoc_sidebar
permalink: mydoc_faq.html
folder: mydoc
---

## FAQs

The psono server offers some commands:


### Do you have an easier way to install Psono and try it out locally?

We offer our [quickstart](https://gitlab.com/psono/psono-quickstart) script that will setup a local demo environment in a couple of minutes.

{% include note.html content="This script is only meant for demo purposes and not for a production grade system." %}

### How to install the enterprise server without docker?

We currently do not provide the possibility to install the enterprise server without docker (baremetal). The baremetal installation
is meant for developers who can deal with python dependency issues and so on. So this has mainly two reasons:

1) The pain to update Psono regulary would be too high for administrators

2) The pain for us to deal with incorrect version libraries or system dependencies is too high for us to support.

### How to resolve "License not provided, and license servers unreachable"?

The psono enterprise server tries to connect to https://license01.psono.com on startup. It will send the PUBLIC_KEY to the server
and the license server responds with a license for 10 users. The error usually means that the server was unable to reach the license server.

You can check that the license server is available by opening this URL [https://license01.psono.com/info/](https://license01.psono.com/info/) in your browser.

This will most likely work indicating some kind of network (DNS, routing, ...) or firewall issue on your server.

You can check if the server in general can reach the license server with this command:

	curl https://license01.psono.com/info/

{% include note.html content="If this command fails, check with your network administrator. Be aware that license01.psono.com sits behind cloudflare so IP whitelisting is impossible." %}

and this one to test from within your docker container:

    docker run psono/psono-server-enterprise:latest curl https://license01.psono.com/info/

{% include note.html content="If the first curl command worked and this one failed, then this is most likely a local missconfiguration. e.g. local firewall or iptables or docker networking issue." %}

If both commands above work, then try with your actual settings.yml

    docker run -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml psono/psono-server-enterprise:latest curl https://license01.psono.com/info/

{% include note.html content="If you receive an error here, then this usually means that you have some kind of problem with your settings.yml. A potential cause could be cryptographic parameters (SECRET, PRIVATE or PUBLIC keys) not being generated with
the `python3 ./psono/manage.py generateserverkeys` command." %}

### Can I run psono server enterprise offline without internet connection?

Yes, yet you have to do some additional configuration.

- Connection to license server: You can ask us to provide you an offline license code. Please send us an email to support@psono.com with your PUBLIC_KEY (it is part of your settings.yml) and we generate you an offline license code.

- Connection to a timeserver: Psono requires a NTP time server to be available. By default it is using time.google.com but it can be adjusted with the TIME_SERVER variable in your settings.yml

- Connection to Yubico Server: If you want to use YubiKey second factor you will have to make sure that your server can reach api*.yubico.com or confugure an own YubiKey server with the YUBICO_API_URLS variable

- Connection to Duo Server: If you want to use Duo second factor you will have to make sure that your server can reach api*.duosecurity.com.

### How do I fix "OCI runtime create failed"-Error?

This error usually has two potential reasons:

1) Incorrect path: Make sure that all paths you have specified in the docker command are correct.

2) Incorrect permissions: Make sure that no file permissions or SELINUX is blocking access to the files you mentioned in your command.

### How to solve that I can only connect to license01 and and not to license02.psono.com?

license02.psono.com currently does not exist. It is configured as a failover server and will be setuped once necessary.
It is the expected result that you can only conenct to license01 and not to license02.psono.com

